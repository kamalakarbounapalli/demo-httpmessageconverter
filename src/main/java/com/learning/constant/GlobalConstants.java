package com.learning.constant;

public interface GlobalConstants {

	String APPLICATION_CSV = "text/csv";
	String APPLICATION_JSON = "application/json";
}
